# Battery Electric Vehicle (BEV) Simulator

Python program that simulates the behavior of electric vehicle users subject to travel, pricing and power policies

## Architecture

![alt-text](doc/BEVSimulator.png)

### Observations:

<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
